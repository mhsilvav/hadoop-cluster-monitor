# ###################################################################################################################
## File:		    cluster-monitor.py
## Description: 	Python Script to Monitor some Cluster metrics
## Author(s): 		Mariano Silva 
## Creation Date:	10-Oct-2020
## Last Update:     05-Mar-2021
## Version:			1.1
## ###################################################################################################################
## Change History
## ###################################################################################################################
## PR	Date	    Author			Description	
## 1.0  10-Oct-2020 Mariano Silva   First release
## 1.1  05-Mar-2021 Mariano Silva   Changed some conditions to send this notification and minor fixes
## ###################################################################################################################

clusterName = "Test Cluster"
YarnURI="http://<ipaddress>:8088/ws/v1/cluster/metrics"

import requests
import json
   
import subprocess
from email.mime.text import MIMEText
from datetime import datetime
import urllib.parse

now = datetime.now()
dt_start = now.strftime("%m/%d/%Y %H:%M:%S")
print("[INFO] Program started at "+str(dt_start))

def send_notif(msg:str):
    now = datetime.now()
    dt_notif = now.strftime("%m/%d/%Y %H:%M:%S")

    txt="At this moment ("+ str(dt_notif) +" UTC), the "+ clusterName +" is showing an unhealthy condition."
    enc_txt=urllib.parse.quote(txt)

    ebody = """At this moment, the cluster is showing an unhealthy condition. See its metrics down below:
    <P>{0}
    <P><font size=-2>
    <br><b>dt_notification:</b> {1}
    </font>
    """
    ts = str(dt_notif)
    
    msg = MIMEText(ebody.format(msg, ts, enc_txt), 'html')
    msg["From"] = "sender@email.com"
    msg["To"] = "recipient1@email.com, recipient2@email.com, recipientN@email.com"
    msg["Subject"] = "[WARNING] "+ clusterName +" is showing an unhealthy condition"
    try:
        p = subprocess.Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=subprocess.PIPE)
        p.communicate(msg.as_bytes())
        print("[INFO] Email successfully sent")
    except:
        print("[ERR] Unexpected error: " ,sys.exc_info())


resp = requests.get(YarnURI)

if resp.status_code != 200:
    raise ApiError('[ERROR]'.format(resp.status_code))
else:
    metrics = resp.json()
    #print(metrics)
    metricsString = json.dumps(metrics)   
    metricsDict = json.loads(metricsString)['clusterMetrics']
 
    n_appsSubmitted = metricsDict['appsSubmitted']
    n_appsCompleted = metricsDict['appsCompleted']
    n_appsPending = metricsDict['appsPending']
    n_appsRunning = metricsDict['appsRunning']
    n_appsFailed = metricsDict['appsFailed']
    
    n_unhealthyNodes = metricsDict['unhealthyNodes']
    n_activeNodes = metricsDict['activeNodes']
    n_memoryUsed_TB = metricsDict['allocatedMB'] / (1024*1024)
    n_memoryTotal_TB = metricsDict['totalMB'] / (1024*1024)
    n_vcoresUsed = metricsDict['allocatedVirtualCores']
    n_vcoresTotal = metricsDict['totalVirtualCores']
    
    ratio_apps=0
    if n_appsRunning>0:
        ratio_apps = n_appsPending/n_appsRunning
    ratio_memory = n_memoryUsed_TB/n_memoryTotal_TB
    ratio_vcores = n_vcoresUsed/n_vcoresTotal
    
    print("[INFO] Ratio pending apps:", ratio_apps)
    print("[INFO] Ratio memory:", ratio_memory)
    print("[INFO] Ratio vcores:", ratio_vcores)
    print("[INFO] Pending apps:", ratio_apps)
    print("[INFO] Unhealthy nodes:", n_appsPending)

    # You can set any condition you want to monitor here
    if ratio_apps>5 or (n_appsPending>0 and n_unhealthyNodes>0) or ratio_memory>0.999 or ratio_vcores>0.999:    
        email_body= "Apps Submitted: "+ str(n_appsSubmitted) 
        email_body= email_body + "<br>Apps Pending: "+ str(n_appsPending) +" ("+str(round(ratio_apps*100,3))+"%)"
        email_body= email_body + "<br>Apps Running: "+ str(n_appsRunning) 
        email_body= email_body + "<br>Apps Completed: "+ str(n_appsCompleted) 
        email_body= email_body + "<br>Apps Failed: "+ str(n_appsFailed) 
        email_body= email_body + "<P>Unhealthy Nodes: "+ str(n_unhealthyNodes) 
        email_body= email_body + "<br>Active Nodes: "+ str(n_activeNodes) 
        email_body= email_body + "<P>Memory Used: "+ str(round(n_memoryUsed_TB,2) ) +"TB ("+str(round(ratio_memory*100,3))+"%)"
        email_body= email_body + "<br>Memory Total: "+ str(round(n_memoryTotal_TB,2) ) +"TB"
        email_body= email_body + "<P>Vcores Used: "+ str(n_vcoresUsed) +" ("+str(round(ratio_vcores*100,3))+"%)"
        email_body= email_body + "<br>Vcores Total: "+ str(n_vcoresTotal) 
        send_notif(email_body)
    else:
        print("[INFO] No email sent")

now = datetime.now()
dt_end = now.strftime("%m/%d/%Y %H:%M:%S")
print("[INFO] Program ended at "+str(dt_end))
## EOF