# Monitoring Utilities

This repo contains a set of utilities for monitoring a Hadoop cluster and other applications, such as Jupyter hub, RStudio and others.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Python 3.x installed. Regular modules such as requests, json, subprocess, urllib installed.
Hadoop 2.x installed.
Sendmail

### Installing

Copy each *.py to the desired location and run it over there.

## Running the tests

Run each file to see everything works well. Example:

```
python File.py
```

## Deployment

You may want to deploy and schedule these utilities via the Linux crontab which is really useful. In order to do that, use these settings:

```
 * * * * *  command-to-execute
 │ │ │ │ │
 │ │ │ │ │
 │ │ │ │ └───── weekday (0 - 6 , 0=Sunday to 6=Saturday)
 │ │ │ └─────── month (1 - 12)
 │ │ └───────── day (1 - 31)
 │ └─────────── hour (0 - 23)
 └───────────── minute (0 - 59)
```

For example, to run the 'File.py' file every 10 minutes, you can use this schedule:
```
*/10 * * * *  cd /path/to/file/ && /path/to/python File.py
```

More examples on scheduling the crontab can be found at https://crontab.guru/examples.html

## Built With

* [Python](https://python.org/) 
* [Visual Studio Code](https://code.visualstudio.com/) 
* [Bitbucket](https://bitbucket.org/)

## Author(s)

* **Mariano Silva** - *Initial work* - [LinkedIn](https://www.linkedin.com/in/masilvav/)

## License

This project is licensed under the MIT License. See the [LICENSE.md](LICENSE.md) file for more details.

## Acknowledgments

* My son who always encourages me to continue 
* My bike rides that inspire me to develop crazy things

[![ForTheBadge powered-by-coffee](http://forthebadge.com/images/badges/powered-by-coffee.svg)](http://ForTheBadge.com)
